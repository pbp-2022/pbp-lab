from django.urls import path
from .views import index

urlpatterns = [
    path('', index, name='index'),
    # TODO Add 'tugas' path using list_tugas Views
]
